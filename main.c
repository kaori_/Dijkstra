#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "input.h"

#define TRUE    1
#define FALSE   0
#define INFINITY   3200
#define MAX_NODE   20

struct connect {
    int id;
    int value;
    struct connect *next;
};

struct node {
    int id;
    int distance;
    int prevNode;
    int isvisted;
    struct connect *c;
    struct node *neighbor;
};

void showNodes(struct node *nodes) {
    printf("Show Nodes-----------\n");
    while (nodes != NULL) {
        printf("id = %d\n", nodes->id);
        printf("prevNode = %d\n", nodes->prevNode);
        while (nodes->c != NULL) {
            printf("%d %d\n", nodes->c->id, nodes->c->value);
            nodes->c = nodes->c->next;
        }
        nodes = nodes->neighbor;
    }
}

void reverseStr(char *str) {
    char temp = '\0';
    char *first = str;
    char *last = str;

    if(*first == '\0'){ return; }

    while( *(last+1) != '\0'){
        last++;
    }

    while(first < last){
        temp = *first;
        *first = *last;
        *last = temp;

        first++;
        last--;
    }
    return;
}

void updateNodeStatus(struct node *pn, struct connect *pc, struct node *lastNode) {
    int d = pc->value+lastNode->distance;

    while (pn->id != pc->id) {
        pn = pn->neighbor;
    }

    if (pn->distance > d) {
        pn->distance = d;
        pn->prevNode = lastNode->id;
    }
}

void checkNodeStatus(struct node *pn, struct node *lastNode) {
    struct connect *pc;
    struct node *start = pn;

    while (pn->id != lastNode->id) {
        pn = pn->neighbor;
    }
    pc = pn->c;

    while (pc != NULL) {
        updateNodeStatus(start, pc, lastNode);
        pc = pc->next;
    }
}

struct node *findNextNode(struct node *pn, struct node *lastNode) {
    struct node *next = NULL;

    while (pn != NULL) {
        if (pn->isvisted == FALSE) {
            next = pn;
            pn = pn->neighbor;
            break;
        }
        pn = pn->neighbor;
    }

    if (next == NULL) {
        // All nodes are visited.
        return NULL;
    }

    while (pn != NULL) {
        if ((pn->isvisted == FALSE) && (next->distance > pn->distance)) {
            next = pn;
        }
        pn = pn->neighbor;
    }
    next->isvisted = TRUE;

    return next;
}

char *findPath(struct node *pn, int sourceId, int destinationId) {
    int numberOfNodes = getNumberOfNodes();
    char *path = malloc(sizeof(char)*numberOfNodes);
    struct node *start = pn;
    int id = destinationId;
    int i = 1;
    path[0] = destinationId;

    while (id != sourceId) {
        while (pn->id != id) {
            pn = pn->neighbor;
        }
        if (pn->prevNode == 0) {
            // The path doesn't exist.
            path = NULL;
            return path;
        }
        path[i] = pn->prevNode;
        id = pn->prevNode;
        pn = start;
        i++;
    }
    path[i] = '\0';

    return path;
}

int getShortestDistance(struct node *pn, int destinationId) {
    int dist = 0;
    while (pn->id != destinationId) {
        pn = pn->neighbor;
    }
    if (pn != NULL) {
        dist = pn->distance;
    }
    return dist;
}

char *dijkstra(struct node *nodes, int sourceId, int destinationId) {
    char *path;
    struct node *lastNode = NULL;
    struct node *pn = nodes, *startpn = nodes;

    // Initialize
    while (pn != NULL) {
        pn->prevNode = 0;
        pn->isvisted = FALSE;
        pn->distance = INFINITY;
        pn = pn->neighbor;
    }

    pn = startpn;

    // About source node
    while (pn->id != sourceId) {
        pn = pn->neighbor;
    }
    pn->distance = 0;
    pn->prevNode = sourceId;
    pn->isvisted = TRUE;
    lastNode = pn;

    do {
        checkNodeStatus(nodes, lastNode);
    } while ((lastNode = findNextNode(nodes, lastNode)) != NULL);

    path = findPath(nodes, sourceId, destinationId);

    return path;

}

// ex)edge = "A_B_10"
void insertConnect(struct node *nodes, char *edge) {
    struct connect c;
    struct connect *start;
    int source = (int)edge[0];
    char *value;

    c.id = (int)edge[2];
    c.next = NULL;
    edge += 4;
    value = edge;
    c.value = strtol(value, NULL, 10);

    while (nodes->id != source) {
        nodes = nodes->neighbor;
    }

    if (nodes->c == NULL) {
        nodes->c = (struct connect *)malloc(sizeof(struct connect));
        start = nodes->c;
    } else {
        start = nodes->c;
        while (nodes->c->next != NULL) {
            nodes->c = nodes->c->next;
        }
        nodes->c->next = (struct connect *)malloc(sizeof(struct connect));
        nodes->c = nodes->c->next;
    }

    nodes->c->id = c.id;
    nodes->c->value = c.value;
    nodes->c->next = NULL;

    nodes->c = start;
}

void createConnect(struct node *nodes) {
    char *strEdges = getEdges();
    char separator = ',';
    char *start = strEdges;
    int i=0;

    while (*strEdges != '\0') {
        strEdges++;
        i++;
        if (strncmp(strEdges, &separator, 1) == 0) {
            char tmpc[i];
            strncpy(tmpc, start, i);
            insertConnect(nodes, tmpc);
            i = 0;
            strEdges++;
            start = strEdges;
        }
        if (*(strEdges+1) == '\0') {
            char tmpc[i+1];
            strncpy(tmpc, start, i+1);
            insertConnect(nodes, tmpc);
            strEdges++;
        }
    }
}

void createNodes(struct node *nodes) {
    char *strNodes = getNodes();
    struct node *start = nodes;
    char separator = ',';

    // The names of the nodes are English alphabets (1 character)
    while (*strNodes != '\0') {
        if (strncmp(strNodes, &separator, 1) != 0) {
            nodes->id = (int)*strNodes;
            nodes->c = NULL;
            if (*(strNodes+1) != '\0') {
                nodes->neighbor = (struct node *)malloc(sizeof(struct node));
                nodes = nodes->neighbor;
            }
        }
        strNodes++;
    }
    nodes->neighbor = NULL;

    nodes = start;
    createConnect(nodes);
}

int checkInput(int source, int destination) {
    char *nodes = getNodes();
    int status = 0;

    if (source < 'A' || source > 'Z' || destination < 'A' || destination > 'Z') {
        return FALSE;
    }

    while (*nodes != '\0') {
        if (*nodes == source) {
            status++;
        }
        if (*nodes == destination) {
            status++;
        }
        nodes++;
    }

    if (status == 2) {
        return TRUE;
    }

    return FALSE;
}

int main() {
    struct node *nodes;
    char *path;
    int source = 0, destination = 0, distance = 0;

    struct node *pn = NULL;
    struct connect *pc = NULL;

    nodes = (struct node *)malloc(sizeof(struct node));
    createNodes(nodes);

    printf("Hello, Dijkstra!\n");
    printf("We have these nodes:\n");
    printf("%s\n", getNodes());
    printf("Enter source node:");
    source = getchar();
    getchar();
    printf("Enter destination node:");
    destination = getchar();

    if (checkInput(source, destination) == FALSE){
        printf("Input Error\n");
        exit(1);
    }

    if (source == destination) {
        printf("Source and Destination are the same!!\n");
        exit(0);
    }

    path = dijkstra(nodes, source, destination);

    if (path == NULL) {
        printf("The path doesn't exist.\n");
    } else {
        reverseStr(path);
        distance = getShortestDistance(nodes, destination);
        printf("The Shortest path %c to %c:\n", source, destination);
        printf("%s\n", path);
        printf("The distance: %d\n", distance);
    }

    // free nodes and edges
    while (nodes != NULL) {
        pn = nodes->neighbor;
        while (nodes->c != NULL) {
            pc = nodes->c->next;
            free(nodes->c);
            nodes->c = pc;
        }
        free(nodes);
        nodes = pn;
    }
    free(path);
    return 0;
}