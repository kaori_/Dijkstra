int getNumberOfNodes() {
    return 7;
}

int getNumberOfEdges() {
    return 8;
}

char *getNodes() {
    return "A,B,C,D,E,F,G"; //these are the nodes of the graph!
}

char *getEdges() {
//    return "A_B_10,A_F_6,B_C_6,C_B_10,D_A_3,E_B_5,F_B_10,G_G_0";
    return "A_B_10,A_F_6,B_C_6,C_B_10,D_A_3,E_B_5,G_B_3,F_B_10";
}
