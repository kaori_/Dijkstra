#ifndef DIJKSTRA_INPUT_H
#define DIJKSTRA_INPUT_H

int getNumberOfNodes();
int getNumberOfEdges();
char *getNodes();
char *getEdges();

#endif //DIJKSTRA_INPUT_H
